
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Reflection;
using WebApplication1;
using WebApplication1.Properties;

var builder = WebApplication.CreateBuilder(args);
//======================================================================

builder.Services.AddDbContext<ApplicationDbContext>(options =>
{
    options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection"));
});



// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo()
    {
        Version = "v1",
        Title = "Post API",
        Description="A wep api sample for post item",
        TermsOfService=new Uri("https://www.example.com/terms"),
        Contact=new OpenApiContact()
        {
            Name="Jiraporn Suwanwong",
            Url=new Uri("https://www.facebook.com/jiraporn")
        },
        License=new OpenApiLicense()
        {
            Name = "Commercial License",
            Url = new Uri("https://www.example.com/licensing")
        }
    });
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFile));
});
/*var apiConfiguration = new ApiConfiguration();
builder.Configuration.Bind("AppSettings:ApiConfiguration");*/
var apiConfiguration=builder.Configuration.GetSection("AppSetting:ApiConfiguration").Get<ApiConfiguration>();
builder.Services.AddSingleton(apiConfiguration);


var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
    app.UseSwagger(options =>
    {
        options.RouteTemplate = "plan/{documentName}/swagger.json";
    });
    app.UseSwaggerUI(options =>
    {
        options.SwaggerEndpoint("/plan/v1/swagger.json", "v1");
        options.RoutePrefix = "plan";
    });
//}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();
using (var scope = app.Services.CreateScope())
{

    var DataContext =scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
    await DataContext.Database.MigrateAsync();
}
app.Run();
