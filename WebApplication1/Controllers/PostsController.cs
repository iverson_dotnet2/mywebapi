﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1;
using WebApplication1.Properties;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly ApiConfiguration _configuration;

        public PostsController(ApplicationDbContext context,ApiConfiguration apiConfiguration)
        {
            _context = context;
            _configuration = apiConfiguration;
        }
        [HttpGet("config")]
        public IActionResult Config()
        {
            return Ok(_configuration);
        }
        /// <summary>
        /// แสดงรายการโพสต์ทั้งหมด
        /// </summary>
        /// <returns>รายการโพสต์ทั้งหมด</returns>
        /// <response code="200">รายการโพสต์</response>
        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK,Type=typeof(IEnumerable<Post>))]
        public async Task<ActionResult<IEnumerable<Post>>> GetPosts()
        {
          if (_context.Posts == null)
          {
              return NotFound();
          }
            return await _context.Posts.ToListAsync();
        }


        /// <summary>
        /// แสดงข้อมูลของโพสต์ตามรหัส
        /// </summary>
        /// <param name="id">รหัสโพสต์</param>
        /// <returns>ข้อมูลของโพสต์</returns>
        /// <response code="200">รายการโพสต์</response>
        /// <response code="400">ไม่พบโพสต์</response>
        [HttpGet("{id}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Post))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Post>> GetPost(int id)
        {
          if (_context.Posts == null)
          {
              return NotFound();
          }
            var post = await _context.Posts.FindAsync(id);

            if (post == null)
            {
                return NotFound();
            }

            return post;
        }


        /// <summary>
        /// สร้างโพสต์ใหม่
        /// </summary>
        /// <param name="Title">หัวข้อเรือง</param>
        /// <param name="Body">เนื้อหาของโพสต์</param>
        /// <returns>ข้อมูลโพสต์</returns>
        /// <response code="201">สร้างโพสต์สำเร็จ</response>
        /// <response code="400">ข้อมูลไม่ถูกต้อง</response>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(Post))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Post>> PostPost(PostDto post)
        {
          if (_context.Posts == null)
          {
              return Problem("Entity set 'ApplicationDbContext.Posts'  is null.");
          }
            var newPost = new Post()
            {
                Title = post.Title,
                Body = post.Body
            };
            _context.Posts.Add(newPost);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPost", new { id = newPost.Id }, newPost);
        }


        private bool PostExists(int id)
        {
            return (_context.Posts?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
