﻿using Microsoft.EntityFrameworkCore;

namespace WebApplication1
{
    public class ApplicationDbContext :DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>().HasKey(p=>p.Id);
            modelBuilder.Entity<Post>().Property(p => p.Id).UseIdentityAlwaysColumn();
        }
    }
}
